# Cyberduck

Ideas

 - [ ] Use Bluetooth keyboard for input
 - [ ] Use 10.1" Tab 2 for display (Portrait if possible)
 - [ ] Add in Pi Zero W or Pi 4 for fancy stuff, Wifi hotspot for tablet.
 - [ ] Add in SSD1306 or other monochrome-pixel display.  Dependent upon Pi
 - [ ] Power supplied via ~2200 mAh 2-3S LiPo hooked into a Buck converter
   - Power will flow into a powered USB hub from which devices will draw power
   - Hard-wire power for keyboard into voltage regulator from buck
   - Lipo charger permanently fixed in place
   - Per-cell voltage measurement to display
 - [ ] Bluetooth mouse for input

Appearance

 - [ ] Tray with plexiglass window covering visible Pi, SSD1306, buck converter and LiPo
 - [ ] Remove case from USB hub, expose inside of case
 - [ ] ws2812b light strip lining interior of box.
 - [ ] Portrait tablet recessed and protected by separate plexi window
 - [ ] Hinged case with stand if toppling is a problem
  
Tracking

 - [Zenkit](https://zenkit.com/collections/LhuGGv28u/views/-woKjVna3m)
 - [public zenkit link](https://public.zenkit.com/collections/LhuGGv28u/views/-woKjVna3m)

Software on Pi
 
  - [ ] ssh
  - [ ] tmux with pre-configured screen divs
  - [ ] git
  - [ ] wordgrinder [info](https://opensource.com/article/18/11/command-line-tools-writers)
  - [ ] Gnu Aspell
  - [ ] Proselint
  - [ ] something for proc info
  - [ ] CLI enhancements
  - [ ] Arduino
  - [ ] vim configured as [here](https://collectiveidea.com/blog/archives/2017/04/05/arduino-programming-in-vim)
  - [ ] Pihole
  - [ ] ~~Gitlab docker instance~~
  - [ ] [netinstall](https://github.com/FooDeas/raspberrypi-ua-netinst)
   
Misc. stuff

CLI RSS reader possibility  
https://gist.github.com/mikedamage/65826  
`./geektool_rss.rb -n 3 -l http://feeds.bbci.co.uk/news/scotland/rss.xml | sed -n 3p | sed 's/[^ ]* //' | cut -c2-45`  
Pi temp  
` sudo /opt/vc/bin/vcgencmd measure_temp | sed 's/temp=//' | sed 's/..$//'`

